import { React, useState } from "react";
import {
  ActivityIndicator,
  Button,
  Image,
  Linking,
  PixelRatio,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TextInput,
  View,
} from "react-native";

export default function App() {
  console.log(PixelRatio.get());
  return <View style={styles.container}></View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
